import request from '@/utils/http'

interface GoodsSkuListModifyListSendModel {
  parentId: number
  price: string | number
  stock_num: string | number
  id?: number
  [propName: string]: any
}
/** 新增或修改商品规格（传了id视为修改）（这里增加规格后端已做已有相同规格判断）
 */
export function goodsSkuListModifyList(data: GoodsSkuListModifyListSendModel) {
  return request({
    url: '/goodsSkuList/modifyList',
    method: 'post',
    data,
  })
}

/** 删除规格列表（可传单个或多个，字符串“,”拼接）
 */
export function goodsSkuListDeleteList(params: { ids: string }) {
  return request({
    url: '/goodsSkuList/deleteList',
    method: 'delete',
    params,
  })
}
