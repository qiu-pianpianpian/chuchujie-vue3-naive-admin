// 文件信息单个item类型
export interface FileItemModel {
  id: string | number
  name: string
  url: string
  type: string
}
