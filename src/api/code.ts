import request from '@/utils/http'

export function getCaptcha() {
  return request<string>({
    url: '/captcha',
    method: 'post',
    noNeedToken: true,
  })
}
