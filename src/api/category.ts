import type { FileItemModel } from './file'
import request from '@/utils/http'

export interface CategoryGetReturnItemModel {
  id: number
  parentId?: number
  icon?: FileItemModel[]
  name: string
  type: string
  createTime?: number
  updateTime?: number
  children?: CategoryGetReturnItemModel[]
}
/**
 * 获取商品一级分类
 */
export function categoryGetOne() {
  return request<CategoryGetReturnItemModel[]>({
    url: '/category/getOne',
    method: 'get',
  })
}

/**
 * 根据一级id获取二级分类（不传为查询全部）
 */
export function categoryGetTwo(params: { parentId?: number } = {}) {
  return request<CategoryGetReturnItemModel[]>({
    url: '/category/getTwo',
    method: 'get',
    params,
  })
}

interface categoryModifySendModel {
  className: string // 传one || two （一级或二级）
  name: string // 名称
  type: string // 类型描述
  id?: string | number
  icon?: string | number
  parentId?: string | number
}
/**
 * 新增或修改一级（二级）分类
 */
export function categoryModify(data: categoryModifySendModel) {
  return request({
    url: '/category/modify',
    method: 'post',
    data,
  })
}
interface CategoryDeleteSendModel {
  id: number
  className: string // 传one || two （一级或二级）
}
/**
 * 删除一级或二级分类
 */
export function categoryDelete(params: CategoryDeleteSendModel) {
  return request({
    url: '/category/delete',
    method: 'delete',
    params,
  })
}
