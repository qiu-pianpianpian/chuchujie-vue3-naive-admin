import type { FileItemModel } from './file'
import request from '@/utils/http'

export interface MyGoodsListSendModel {
  page: number
  size: number
  categoryTwoId?: number | null
  value?: string | null
  total?: number
}
interface MyGoodsListReturnModel<T = GoodsItemModel> {
  total: number
  pageNum: number
  page: number
  size: number
  list: T[]
}

/** 根据二级分类id查询我的商品列表（不传为查询全部）
 */
export function myGoodsList(data: MyGoodsListSendModel) {
  return request<MyGoodsListReturnModel>({
    url: '/myGoods/list',
    method: 'post',
    data,
  })
}
/** 根据商品id查询商品详情
 */
export function myGoodsDetail(data: { goodsId: number | string }) {
  return request<GoodsItemModel>({
    url: '/myGoods/detail',
    method: 'post',
    data,
  })
}

export interface MyGoodsModifySendModel {
  id?: number | string
  parentId: number | string | null
  name: string
  iconId: string
  homeImageIds: string
  detailsImageIds: string
  hot: string
  stock_num: number | null
  price: number | null
  oldPrice: number | null
  none_sku: boolean
}
/** 修改或发布商品（传id为修改商品，不传则增加商品）
 */
export function myGoodsModify(data: MyGoodsModifySendModel) {
  return request<GoodsItemModel>({
    url: '/myGoods/modify',
    method: 'post',
    data,
  })
}

// 商品子项item类型
export interface GoodsItemModel<F=FileItemModel, L=GoodsItemListModel, S=GoodsItemskuListModel> {
  id: number
  parentId: number | string
  adminId: number
  name: string
  hot: string
  stock_num: number
  price: string
  oldPrice: string
  sku: string
  none_sku: number
  createTime: number
  updateTime: number
  iconId: F[]
  homeImageIds: F[]
  detailsImageIds: F[]
  skuList: S[]
  list?: L[]
}

// 商品规格列表类型
interface GoodsItemListModel {
  id: number
  parentId: number
  price: string
  stock_num: number
  createTime: number
  updateTime: number
  [propName: string]: any
}

// 商品规格信息类型
export interface GoodsItemskuListModel<V=GoodsItemskuListVModel> {
  id: number
  k: string
  k_s: string
  largeImageMode: boolean
  v: V[]
  createTime: number
  updateTime: number
  vId?: number // 用于组件实例
}

// 商品规格信息item子项类型
export interface GoodsItemskuListVModel<F=FileItemModel> {
  id: number
  skuId: number
  parentId: number
  name: string
  imgUrl: F[]
  previewImgUrl: F[]
  createTime: string
  updateTime: string
}
