import request from '@/utils/http'

export default {
  getUser: () => request({
    url: '/user',
    method: 'get',
  }),
  refreshToken: () => request({
    url: '/auth/refreshToken',
    method: 'post',
  }),
}
