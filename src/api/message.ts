import request from '@/utils/http'

export interface MessageGetListSendModel {
  page: number
  size: number
  type?: string
  total?: number
}
export interface MessageGetListReturnData {
  id: number
  name: string
  isShow: boolean
  type: string
  createTime?: number | null
  updateTime?: number | null
}
interface MessageGetListReturnModel<T = MessageGetListReturnData> {
  total: number
  pageNum: number
  page: number
  size: number
  list: T[]
}
/** 获取系统公告列表
 */
export function messageGetList(params: MessageGetListSendModel) {
  return request<MessageGetListReturnModel>({
    url: '/message/getList',
    method: 'get',
    params,
  })
}

/** 删除通知
 */
export function messageDelete(params: { id: number }) {
  return request({
    url: '/message/delete',
    method: 'delete',
    params,
  })
}

/** 修改或新增通知（只会使用一个通知）
 */
export function messageModifyShow(data: MessageGetListReturnData) {
  return request({
    url: '/message/modifyShow',
    method: 'post',
    data,
  })
}

