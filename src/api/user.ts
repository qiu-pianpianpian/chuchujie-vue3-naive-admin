import request from '@/utils/http'

export interface UserLoginSendModel {
  phone: string | number
  password: string
  captcha: string
}
/** 用户登录
 */
export function userLogin(data: UserLoginSendModel) {
  return request<string>({
    url: '/user/login',
    method: 'post',
    data,
    noNeedToken: true,
  })
}

export interface userGetuserInfoReturnModel {
  id: number
  userName: string
  phone: string
  email: string
  avatar: string
  password: string
  descText: string
  addressName: string
  province: string
  city: string
  county: string
  addressDetail: string
  createTime: number
  updateTime: number
  rate_count: string
}
/** 获得用户信息
 */
export function userGetuserInfo() {
  return request<userGetuserInfoReturnModel>({
    url: '/user/getInfo',
    method: 'get',
  })
}

/** 刷新token
 */
export function userRefreshToken() {
  return request<string>({
    url: '/user/refreshToken',
    method: 'get',
  })
}
