import type { Router } from 'vue-router'
import { getToken, refreshAccessToken } from '@/utils/auth/token'
import { isNullOrWhitespace } from '@/utils/common'

const WHITE_LIST = ['/login'] // 白名单
export function createPermissionGuard(router: Router) {
  router.beforeEach(async (to) => {
    const token = getToken()

    /** 没有token的情况 */
    if (isNullOrWhitespace(token)) {
      if (WHITE_LIST.includes(to.path))
        return true

      return { path: 'login', query: { ...to.query, redirect: to.path } }
    }

    /** 有token的情况 */
    if (to.path === '/login')
      return { path: '/' }

    refreshAccessToken()
    return true
  })
}
