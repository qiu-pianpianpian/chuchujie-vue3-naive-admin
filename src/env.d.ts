// ts 环境变量类型
// https://vitejs.cn/guide/env-and-mode.html#intellisense

declare interface ImportMetaEnv {
  readonly VITE_APP_TITLE: string
  readonly VITE_PORT: string
  readonly VITE_BASE_API: string
  readonly VITE_PUBLIC_PATH: string
  readonly VITE_USE_HASH: boolean
  readonly VITE_USE_MOCK: boolean
  // 更多环境变量...
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
