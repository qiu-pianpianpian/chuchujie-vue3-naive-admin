import type { RouteType } from '~/types/router'
const Layout = () => import('@/layout/index.vue')

export default {
  name: 'category',
  path: '/category',
  component: Layout,
  redirect: '/category/list',
  meta: {
    order: 99,
  },
  children: [
    {
      name: 'categoryList',
      path: 'list',
      component: () => import('./list/index.vue'),
      meta: {
        title: '分类管理',
        icon: 'bx:category',
      },
    },
  ],
} as RouteType
