import type { RouteType } from '~/types/router'
const Layout = () => import('@/layout/index.vue')

export default {
  name: 'Goods',
  path: '/Goods',
  component: Layout,
  redirect: '/Goods/List',
  meta: {
    order: 2,
    title: '商品管理',
    icon: 'ep:goods-filled',
  },
  children: [
    {
      name: 'GoodsList',
      path: 'List',
      component: () => import('./list/index.vue'),
      meta: {
        title: '商品列表',
        icon: 'material-symbols:format-list-bulleted',
      },
    },
    {
      path: 'Add',
      name: 'GoodsAdd',
      props: route => ({
        goodsId: route.params.goodsId,
      }),
      component: () => import('./modify/index.vue'),
      meta: {
        title: '商品发布',
        icon: 'ic:round-add-box',
      },
    },
    {
      path: 'Modify/:goodsId',
      name: 'GoodsModify',
      props: route => ({
        goodsId: route.params.goodsId,
      }),
      component: () => import('./modify/index.vue'),
      isHidden: true,
      meta: {
        title: '商品修改',
      },
    },
  ],
} as RouteType
