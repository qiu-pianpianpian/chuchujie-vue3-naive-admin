import type { RouteType } from '~/types/router'
const Layout = () => import('@/layout/index.vue')

export default {
  name: 'Dashboard',
  path: '/',
  component: Layout,
  redirect: '/homePage',
  meta: {
    order: 0,
  },
  children: [
    {
      name: 'homePage',
      path: 'homePage',
      component: () => import('./index.vue'),
      meta: {
        title: '首页',
        icon: 'mdi:home',
      },
    },
  ],
} as RouteType
