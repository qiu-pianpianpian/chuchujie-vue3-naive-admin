import type { RouteType } from '~/types/router'
const Layout = () => import('@/layout/index.vue')

export default {
  name: 'noticeAdmin',
  path: '/notice',
  component: Layout,
  redirect: '/notice/admin',
  meta: {
    order: 1,
  },
  children: [
    {
      name: 'NoticeAdminPage',
      path: 'admin',
      component: () => import('./index.vue'),
      meta: {
        title: '通知管理',
        icon: 'mdi:bullhorn-variant',
      },
    },
  ],
} as RouteType
