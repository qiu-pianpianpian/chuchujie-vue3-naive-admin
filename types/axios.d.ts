import type { AxiosRequestConfig, AxiosRequestHeaders } from 'axios'

type RequestHeader = AxiosRequestHeaders & { token?: string }

export interface RequestConfig<T = RequestHeader> extends AxiosRequestConfig {
  headers?: T
  /** 接口是否需要token */
  noNeedToken?: boolean
  /** 接口是否需要错误提醒 */
  noNeedTip?: boolean
}

interface ErrorResolveResponse {
  code?: number | string
  message: string
  data?: any
}